<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use InvalidArgumentException;
use PhpExtended\Parser\ParserInterface;
use Stringable;

/**
 * ReifierConfigurationInterface interface file.
 * 
 * The reifier configuration gathers all additional data that may help reify
 * different objects from different data structures.
 * 
 * FQCN stands for Fully Qualified Class Name, and should be class names with
 * their full namespaces. Any class with no namespace attached is interpreted
 * as if they are in the generic root / core namespace.
 * 
 * @author Anastaszor
 */
interface ReifierConfigurationInterface extends Stringable
{
	
	/**
	 * Enables the inference for implementation classname lookups with
	 * interface class names beginning with "I". Removing the leading "I"
	 * gives the class name to look after.
	 * 
	 * @return ReifierConfigurationInterface
	 */
	public function enableInferImplementationClassnameFromIPrefix() : ReifierConfigurationInterface;
	
	/**
	 * Disables the inference for implementation class name lookup with
	 * interface class names beginning with "I".
	 * 
	 * @return ReifierConfigurationInterface
	 */
	public function disableInferImplementationClassnameFromIPrefix() : ReifierConfigurationInterface;
	
	/**
	 * Gets whether to look after the class names by removing the leading "I"
	 * when encountering an interface name that begins with it.
	 * 
	 * @return boolean
	 */
	public function isInferImplementationClassnameFromIPrefixEnabled() : bool;
	
	/**
	 * Enables the inference for implementation classname lookups with
	 * interface class names ending with "Interface". Removing the trailing
	 * "Interface" gives the class name to look after.
	 * 
	 * @return ReifierConfigurationInterface
	 */
	public function enableInferImplementationClassnameFromInterfaceSuffix() : ReifierConfigurationInterface;
	
	/**
	 * Disables the inference for implementation class name lookup with
	 * interface class names ending with "Interface".
	 * 
	 * @return ReifierConfigurationInterface
	 */
	public function disableInferImplementationClassnameFromInterfaceSuffix() : ReifierConfigurationInterface;
	
	/**
	 * Gets whether to look after the class names by removing the trailing
	 * "Interface" when encountering an interface name that ends with it.
	 * 
	 * @return boolean
	 */
	public function isInferImplementationClassnameFromInterfaceSuffixEnabled() : bool;
	
	/**
	 * Sets whether to ignore excess fields with the given names for a given
	 * class.
	 * 
	 * @param class-string $fqcn
	 * @param array<integer, string> $fieldNames
	 * @return ReifierConfigurationInterface
	 */
	public function addIgnoreExcessFields(string $fqcn, array $fieldNames) : ReifierConfigurationInterface;
	
	/**
	 * Sets whether to ignore excess field with the given names for a given
	 * class.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return ReifierConfigurationInterface
	 */
	public function addIgnoreExcessField(string $fqcn, string $fieldName) : ReifierConfigurationInterface;
	
	/**
	 * Whether to ignore excess fields (fields that are in the array for a
	 * given object for the given class name, but are not available as public
	 * field, in constructor parameters nor in setter). 
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return boolean
	 */
	public function mustIgnoreExcessField(string $fqcn, string $fieldName) : bool;
	
	/**
	 * Gets the excess fields that are ignored, indexed by class name.
	 * 
	 * @return array<class-string, array<integer, string>>
	 */
	public function getAllIgnoredExcessFields() : array;
	
	/**
	 * Sets whether to ignore failed reification of values for the class with
	 * the given fqcn and for the given fields names.
	 *
	 * @param class-string $fqcn
	 * @param array<integer, string> $fieldNames
	 * @return ReifierConfigurationInterface
	 */
	public function addFieldsAllowedToFail(string $fqcn, array $fieldNames) : ReifierConfigurationInterface;
	
	/**
	 * Sets whether to ignore failed reification of values for the class with
	 * the given fqcn and for the given field name.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return ReifierConfigurationInterface
	 */
	public function addFieldAllowedToFail(string $fqcn, string $fieldName) : ReifierConfigurationInterface;
	
	/**
	 * Whether the field of the given fieldName for the given class is failable,
	 * meaning if non-null data is given but the parsing or reificiation fails
	 * for any reason. For iterable field names, having it failable means that
	 * any inner object may fail.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return boolean
	 */
	public function isFieldAllowedToFail(string $fqcn, string $fieldName) : bool;
	
	/**
	 * Gets the fields that are allowed to fail to be parsed.
	 *
	 * @return array<class-string, array<integer, string>>
	 */
	public function getAllFieldsAllowedToFail() : array;
	
	/**
	 * Sets whether to interpret empty strings of values for the class with
	 * the given fqn and for the given fields names as null values.
	 * 
	 * @param class-string $fqcn
	 * @param array<integer, string> $fieldNames
	 * @return ReifierConfigurationInterface
	 */
	public function addFieldsEmptyAsNull(string $fqcn, array $fieldNames) : ReifierConfigurationInterface;
	
	/**
	 * Sets whether to interpret empty strings of values for the class with
	 * the given fqcn and for the given field name as null values.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return ReifierConfigurationInterface
	 */
	public function addFieldEmptyAsNull(string $fqcn, string $fieldName) : ReifierConfigurationInterface;
	
	/**
	 * Whether the field of the given fieldName for the given class will be
	 * considered as null if it is represented as an empty string. If such
	 * field is nullable and an empty string is given, it should not be passed
	 * to the parser, and should be interpreted as a null value instead.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return boolean
	 */
	public function isFieldEmptyAsNull(string $fqcn, string $fieldName) : bool;
	
	/**
	 * Gets the fields that are allowed to be interpreted as null if empty.
	 *
	 * @return array<class-string, array<integer, string>>
	 */
	public function getAllFieldsEmptyAsNull() : array;
	
	/**
	 * Sets an alias for the given field name in of the given class. This
	 * allows a constructor parameter name as fieldName, or a field name of 
	 * the class, piloted by itself if it is public, or by its getter or setter
	 * to have an alternate name to search in the keys of the data arrays.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @param string $alias
	 * @return ReifierConfigurationInterface
	 */
	public function addFieldNameAlias(string $fqcn, string $fieldName, string $alias) : ReifierConfigurationInterface;
	
	/**
	 * Gets the alias of the given field name, or returns the field name
	 * unchanged if there are no aliases defined.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return string
	 */
	public function getAliasFromFieldName(string $fqcn, string $fieldName) : string;
	
	/**
	 * Gets the field name from the given alias, or returns the alias unchanged
	 * if there are no field names associated to that alias.
	 * 
	 * @param class-string $fqcn
	 * @param string $alias
	 * @return string
	 */
	public function getFieldNameFromAlias(string $fqcn, string $alias) : string;
	
	/**
	 * Gets the fields that are aliased, indexed by classname and field name.
	 * 
	 * @return array<class-string, array<string, string>>
	 */
	public function getAllFieldNameAliases() : array;
	
	/**
	 * Sets the formats that should be tried to parse core DateTimeInterface,
	 * DateTime and DateTimeImmutable objects for the given concrete class and
	 * all of its given fields.
	 * 
	 * @param class-string $fqcn
	 * @param array<integer, string> $fieldNames
	 * @param array<integer, string> $formats
	 * @return ReifierConfigurationInterface
	 */
	public function addDateTimeFormats(string $fqcn, array $fieldNames, array $formats) : ReifierConfigurationInterface;
	
	/**
	 * Sets the formats that should be tried to parse core DateTimeInterface,
	 * DateTime and DateTimeImmutable objects for the given concrete class and
	 * the given field.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @param array<integer, string> $formats
	 * @return ReifierConfigurationInterface
	 */
	public function addDateTimeFormat(string $fqcn, string $fieldName, array $formats) : ReifierConfigurationInterface;
	
	/**
	 * Gets the formats that should be tried to parse core DateTimeInterface,
	 * DateTime and DateTimeImmutable objects for the given concrete class
	 * and its field name.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return array<integer, string>
	 */
	public function getDateTimeFormats(string $fqcn, string $fieldName) : array;
	
	/**
	 * Gets the datetime formats, indexed by class name and field name.
	 * 
	 * @return array<class-string, array<string, array<integer, string>>>
	 */
	public function getAllDateTimeFormats() : array;
	
	/**
	 * Sets the inner type of objects inside an iterable argument for the given
	 * object class and the given field names. The $attributeInnerClass will be
	 * the resolved inner class for generics.
	 * 
	 * @param class-string $fqcn
	 * @param array<integer, string> $fieldNames
	 * @param class-string|'bool'|'int'|'float'|'string'|'array' $attributeInnerClass
	 * @return ReifierConfigurationInterface
	 */
	public function setIterableInnerTypes(string $fqcn, array $fieldNames, string $attributeInnerClass) : ReifierConfigurationInterface;
	
	/**
	 * Sets the inner type of objects inside an iterable argument for the given
	 * object class and the given field name. The $attributeInnerClass will be
	 * the resolved inner class for generics.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @param class-string|'bool'|'int'|'float'|'string'|'array' $attributeInnerClass
	 * @return ReifierConfigurationInterface
	 */
	public function setIterableInnerType(string $fqcn, string $fieldName, string $attributeInnerClass) : ReifierConfigurationInterface;
	
	/**
	 * Gets the inner type of objects inside an iterable argument for the given
	 * object class and the field to resolve. If no such inner type can be
	 * found, a MissingImplementationThrowable is thrown.
	 * 
	 * @param class-string $fqcn
	 * @param string $fieldName
	 * @return class-string|'bool'|'int'|'float'|'string'|'array'
	 * @throws MissingInnerTypeThrowable
	 */
	public function getIterableInnerType(string $fqcn, string $fieldName) : string;
	
	/**
	 * Gets the iterable inner types, indexed by class name and field name.
	 * 
	 * @return array<class-string, array<string, class-string|'bool'|'int'|'float'|'string'|'array'>>
	 */
	public function getAllIterableInnerTypes() : array;
	
	/**
	 * Sets the class that correspond to a specific key-value pair to be used
	 * in case of polymorphic resolution. This resolution has higher priority
	 * than the key-presence resolution.
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn the interface to resolve
	 * @param integer $priority 0,1,2...
	 * @param string $key the key to base judgment on
	 * @param string $value the value to match
	 * @param class-string<T> $innerClassFqcn the class to instanciate
	 * @return ReifierConfigurationInterface
	 * @throws InvalidArgumentException if the inner class does not extends outer
	 */
	public function addPolymorphism(string $fqcn, int $priority, string $key, string $value, string $innerClassFqcn) : ReifierConfigurationInterface;
	
	/**
	 * Gets all the polymorphisms key-value for the given interface or mother
	 * class.
	 * 
	 * Returns [priority => [key => [value => child-class]]]
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn
	 * @return array<integer, array<string, array<string, class-string<T>>>>
	 */
	public function getPolyMorphismsFor(string $fqcn) : array;
	
	/**
	 * Gets all the polymorphisms key-value that are in this configuration.
	 * 
	 * Returns [mother-class => [priority => [key => [value => child-class]]]]
	 * 
	 * @return array<class-string, array<integer,array<string, array<string, class-string>>>>
	 */
	public function getAllPolymorphisms() : array;
	
	/**
	 * Sets the class that correspond to a specific key (to be present) to be
	 * used in case of polymorphic resolution. This resolution has less priority
	 * than the key-value resolution.
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn the interface to resolve
	 * @param integer $priority 0,1,2...
	 * @param string $key the key to base judgment on
	 * @param class-string<T> $innerClassFqcn the class to instanciate
	 * @return ReifierConfigurationInterface
	 * @throws InvalidArgumentException if the inner class does not extends outer
	 */
	public function addPolymorphismPresence(string $fqcn, int $priority, string $key, string $innerClassFqcn) : ReifierConfigurationInterface;
	
	/**
	 * Gets all the polymorphisms for the given interface or mother class.
	 * 
	 * Returns [priotiry => [key => child-class]]
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn
	 * @return array<integer, array<string, class-string<T>>>
	 */
	public function getPolymorphismsPresenceFor(string $fqcn) : array;
	
	/**
	 * Gets all the polymorphisms presence that are in this configuration.
	 * 
	 * Returns [mother-class => [priority => [key => child-class]]]
	 * 
	 * @return array<class-string, array<integer, array<string, class-string>>>
	 */
	public function getAllPolymorphismsPresence() : array;
	
	/**
	 * Sets the parser object for the given class string. The given $fqcn
	 * class string can be a concrete or abstract class or an interface name.
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn
	 * @param ParserInterface<T> $parser
	 * @return ReifierConfigurationInterface
	 */
	public function setParser(string $fqcn, ParserInterface $parser) : ReifierConfigurationInterface;
	
	/**
	 * Gets the parser object for the given class name. If the given class name
	 * is an abstract class or an interface, it will be resolved against its
	 * implementation. If there is no suitable parser available for the
	 * resolved concrete class name, a MissingParserThrowable is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $fqcn
	 * @return ParserInterface<T>
	 * @throws MissingParserThrowable
	 */
	public function getParser(string $fqcn) : ParserInterface;
	
	/**
	 * Gets the parsers, indexed by class string.
	 *
	 * @template T of object
	 * @return array<class-string<T>, ParserInterface<T>>
	 * @phpstan-ignore-next-line
	 */
	public function getAllParsers() : array;
	
	/**
	 * Sets the implementation class for the given interface class. If the
	 * $interfaceFqcn argument does not correspond to an existing interface or
	 * abstract class, or if the $classFqcn argument does not correspond to a
	 * non-abstract class, or if the $classFqcn does not implements or extends
	 * the $interfaceFqcn, an InvalidArgumentException is thrown.
	 * 
	 * An Implementation may also be used as default resolution on a polymorphic
	 * class resolution with lowest priority.
	 * 
	 * @template T of object
	 * @param class-string<T> $interfaceFqcn
	 * @param class-string<T> $classFqcn
	 * @return ReifierConfigurationInterface
	 * @throws InvalidArgumentException
	 */
	public function setImplementation(string $interfaceFqcn, string $classFqcn) : ReifierConfigurationInterface;
	
	/**
	 * Gets the implementation class for the given interface class. If no such
	 * implementation exists, then a MissingImplementationThrowable is thrown,
	 * unless the given $interfaceFqcn is actually a concrete class that is
	 * its own implementation.
	 * 
	 * @template T of object
	 * @param class-string<T> $interfaceFqcn
	 * @return class-string<T>
	 * @throws MissingImplementationThrowable
	 */
	public function getImplementation(string $interfaceFqcn) : string;
	
	/**
	 * Gets the implementations, indexed by class name.
	 * 
	 * @return array<class-string, class-string>
	 */
	public function getAllImplementations() : array;
	
	/**
	 * Builds a new ReifierConfigurationInterface by adding the settings of
	 * this configuration and the settings of the other configuration. In case
	 * of conflict, the settings of the other configuration prevail.
	 * 
	 * @param ReifierConfigurationInterface $other
	 * @return ReifierConfigurationInterface
	 */
	public function mergeWith(?ReifierConfigurationInterface $other) : ReifierConfigurationInterface;
	
}
