<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Throwable;

/**
 * MissingImplementationThrowable class file.
 * 
 * Missing Implementation Throwable are thrown when the engine encounters an
 * argument type that is given with an interface or abstract class type hint,
 * and that it must choose which object of a concrete class it should give that
 * implements that interface or extends that abstract class, and it cannot
 * because none are specified.
 * 
 * @author Anastaszor
 */
interface MissingImplementationThrowable extends Throwable
{
	
	/**
	 * Gets the depths in which the reification failed.
	 *
	 * @return integer
	 */
	public function getDepths() : int;
	
	/**
	 * Gets the full path from the root object to the failed attribute.
	 *
	 * @return string
	 */
	public function getPath() : string;
	
	/**
	 * Returns the subtree that failed the reification process.
	 *
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function getData();
	
	/**
	 * Gets the expected class for reification.
	 *
	 * @return class-string
	 */
	public function getExpectedClass() : string;
	
	/**
	 * Gets the failed attribute for reification.
	 *
	 * @return string
	 */
	public function getFailedAttribute() : string;
	
}
