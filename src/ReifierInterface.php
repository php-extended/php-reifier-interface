<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Iterator;
use Stringable;

/**
 * ReifierInterface interface file.
 * 
 * This interface defines an engine that can turn recursive array-based data
 * structures into similarly recursive object-based data structures.
 * 
 * @author Anastaszor
 */
interface ReifierInterface extends Stringable
{
	
	/**
	 * Sets the configuration that will be used to reify data structures.
	 * 
	 * @param ?ReifierConfigurationInterface $config
	 * @return ReifierInterface
	 */
	public function setConfiguration(?ReifierConfigurationInterface $config) : ReifierInterface;
	
	/**
	 * Gets the configuration that is used by this reifier.
	 * 
	 * @return ReifierConfigurationInterface
	 */
	public function getConfiguration() : ReifierConfigurationInterface;
	
	/**
	 * Transforms a recursive array-based data structure into a recursive
	 * object-based data structure with the one object of wanted class at the
	 * top of the tree. If the reification cannot be done, then a
	 * ReificationThrowable is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @return T
	 * @throws ReificationThrowable if the reification fails
	 */
	public function reify(string $objectClass, array $data) : object;
	
	/**
	 * Transforms a recursive array-based data structure into a recursive
	 * object-based data structure with the one object of wanted class at the
	 * top of the tree. If a nullable value is represented, then a null is
	 * returned, otherwise if the reification cannot be done, a
	 * ReificationThrowable is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @return ?T
	 * @throws ReificationThrowable if the reification fails
	 */
	public function reifyNullable(string $objectClass, array $data) : ?object;
	
	/**
	 * Transforms a recursive array-based data structure into a recursive
	 * object-based data structure with the one object of wanted class at the
	 * top of the tree. If the reificaation cannot be done, then a null is
	 * returned.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ?ReifierReportInterface $report
	 * @param integer $idx the index when used in loop
	 * @return ?T
	 */
	public function tryReify(string $objectClass, array $data, ?ReifierReportInterface $report = null, int $idx = 0) : ?object;
	
	/**
	 * Transforms a recursive array-based data structure into a recursive
	 * object-based data structure with the one object of wanted class at the
	 * top of the tree. If a nullable value is represented, then a null is
	 * returned, otherwise if the reification cannot be done, a null is also
	 * returned.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $data
	 * @param ?ReifierReportInterface $report
	 * @param integer $idx the index when used in loop
	 * @return ?T
	 */
	public function tryReifyNullable(string $objectClass, array $data, ?ReifierReportInterface $report = null, int $idx = 0) : ?object;
	
	/**
	 * Transforms all the recursive array-based data structures into a recursive
	 * object-based data structure with the one object wanted class at the top
	 * of the tree. If the reification of one object cannot be done, then a
	 * ReificationThrowable is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @return array<integer, T>
	 * @throws ReificationThrowable
	 */
	public function reifyAll(string $objectClass, array $datas) : array;
	
	/**
	 * Transforms all the recursive array-based data structures into a recursive
	 * object-based data structure with the one object wanted class at the top
	 * of the tree. If a nullable value is represented, then it is omitted from
	 * the returned array, otherwise if the reification of one object cannot be
	 * done, a ReificationThrowable is thrown.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @return array<integer, T>
	 * @throws ReificationThrowable
	 */
	public function reifyAllNullable(string $objectClass, array $datas) : array;
	
	/**
	 * Tries to transform all the recursive array-based data structures into a
	 * recursive object-based data structure with the one object wanted class
	 * at the top of the tree. If the reification of one object cannot be done,
	 * then it is omitted from the returned array.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ?ReifierReportInterface $report
	 * @return array<integer, T>
	 */
	public function tryReifyAll(string $objectClass, array $datas, ?ReifierReportInterface $report = null) : array;
	
	/**
	 * Tries to transform all the recursive array-based data structures into a
	 * recursive object-based data structure with the one object wanted class
	 * at the top of the tree. If a nullable value is represented, or if the
	 * reification of one object cannot be done, then it is omitted from the
	 * returned array.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param array<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $datas
	 * @param ?ReifierReportInterface $report
	 * @return array<integer, T>
	 */
	public function tryReifyAllNullable(string $objectClass, array $datas, ?ReifierReportInterface $report = null) : array;
	
	/**
	 * Returns an iterator that transforms on the fly all recursive array-based
	 * data structures into recursive object-based data structures with the
	 * one object wanted class at the top of the tree. If the reification of one
	 * object cannot be done, then a ReificationThrowable will be thrown by the
	 * iterator when constructing the objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param Iterator<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $iterator
	 * @return Iterator<integer, T>
	 */
	public function reifyIterator(string $objectClass, Iterator $iterator) : Iterator;
	
	/**
	 * Returns an iterator that transforms on the fly all recursive array-based
	 * data structures into recursive object-based data structures with the
	 * one object wanted class at the top of the tree. If a nullable value is
	 * represented, then it will be omitted from the iteration, otherwise if the
	 * reification of one object cannot be done, then a ReificationThrowable
	 * will be thrown by the iterator when constructing the objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param Iterator<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $iterator
	 * @return Iterator<integer, T>
	 */
	public function reifyIteratorNullable(string $objectClass, Iterator $iterator) : Iterator;
	
	/**
	 * Returns an iterator that transforms on the fly all recursive array-based
	 * data structures into recursive object-based data structures with the
	 * one object wanted class at the top of the tree. If the reification of one
	 * object cannot be done, then it will be omitted from the iteration.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param Iterator<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $iterator
	 * @param ?ReifierReportInterface $report
	 * @return Iterator<integer, T>
	 */
	public function tryReifyIterator(string $objectClass, Iterator $iterator, ?ReifierReportInterface $report = null) : Iterator;
	
	/**
	 * Returns an iterator that transforms on the fly all recursive array-based
	 * data structures into recursive object-based data structures with the
	 * one object wanted class at the top of the tree. If a nullable value is
	 * represented, or if the reification of one object cannot be done, then it
	 * will be omitted from the iteration.
	 * 
	 * If a reifier report is given, it will be filled with all the errors
	 * that are encountered when reifiying those objects.
	 * 
	 * @template T of object
	 * @param class-string<T> $objectClass
	 * @param Iterator<integer|string, array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $iterator
	 * @param ?ReifierReportInterface $report
	 * @return Iterator<integer, T>
	 */
	public function tryReifyIteratorNullable(string $objectClass, Iterator $iterator, ?ReifierReportInterface $report = null) : Iterator;
	
}
