<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Countable;
use Iterator;
use Stringable;

/**
 * ReifierReportInterface interface file.
 * 
 * This is a report on reification errors that occured during the reification
 * process.
 * 
 * @author Anastaszor
 * @extends \Iterator<integer, ReifierReportEntryInterface>
 */
interface ReifierReportInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * Gets information about the source of data that is used to make this
	 * report. The source is human readable information like the uri of the
	 * resource that provided data.
	 * 
	 * @return string
	 */
	public function getSource() : string;
	
	/**
	 * Adds a new entry to the list of entries for this report.
	 * 
	 * @param ReifierReportEntryInterface $entry
	 * @return ReifierReportInterface
	 */
	public function addEntry(ReifierReportEntryInterface $entry) : ReifierReportInterface;
	
	/**
	 * Removes all the entries of this report. If a classname is given, then
	 * removes only the entries that were reported for the given classname.
	 * 
	 * @param string $classname
	 * @return integer
	 */
	public function clear(?string $classname = null) : int;
	
}
