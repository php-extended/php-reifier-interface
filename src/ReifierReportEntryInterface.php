<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-reifier-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Reifier;

use Stringable;

/**
 * ReifierReportEntryInterface interface file.
 * 
 * This represents an entry of a failure to be reified for a given chunk of
 * data. This object remembers information about the chunk of data that failed
 * reification for later analysis.
 * 
 * @author Anastaszor
 */
interface ReifierReportEntryInterface extends Stringable
{
	
	/**
	 * Gets the index of the object that failed reification into the iteration.
	 * 
	 * @return integer
	 */
	public function getIndex() : int;
	
	/**
	 * Gets the internal path into the data structure to find the object that
	 * failed reification.
	 * 
	 * @return string
	 */
	public function getPath() : string;
	
	/**
	 * Gets the offset on the data that failed parsing through the original
	 * data structure.
	 * 
	 * @return integer
	 */
	public function getOffset() : int;
	
	/**
	 * Get a string representation of the data that failed reification. This
	 * data is not made to be used automatically, but as help for humans to
	 * find the right problem when the reification was done and has failed.
	 * 
	 * @return ?string
	 */
	public function getData() : ?string;
	
	/**
	 * Gets the class of the object that failed to be parsed.
	 * 
	 * @return string
	 */
	public function getClassname() : string;
	
	/**
	 * Gets the message of the reification failure.
	 * 
	 * @return string
	 */
	public function getMessage() : string;
	
}
